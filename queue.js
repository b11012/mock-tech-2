let collection = [];

// Write the queue functions below.

class Queue {

	constructor() {
		this.elements = {};
		this.head = 0;
		this.tail = 0;
	}

	enqueue(element) {
		this.elements[this.tail] = element;
		this.tail++;
	}

	dequeue() {
		const item = this.elements[this.head];
		delete this.elements[this.head];
		this.head++;
		return item;
	}

	peek() {
	  return this.elements[this.head];
	}

	get length() {
	  return this.tail - this.head;
	}

	get isEmpty() {
	  return this.tail - this.head;
	}
}

let q = new Queue();
for (let i = 1; i <= 7; i++) {
  q.enqueue(i);
 }


module.exports = {
	q.enqueue
	q.dequeue()
	q.peek()
	q.length()
	q.isEmpty()
};